//Name:Brandon Mathis
//File Name:desencrypt.cpp
//Date:April 21, 2016
//Program Description:Encryption/Decryption using DES

#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <iomanip>
#include <math.h>

using namespace std;
int getPlainText(ifstream& input, string& plainText);
void convertKeyToArray(string key, int keyInBits[],int start);
void convertMessageToArray(string message, int messageInBits[],int start, int end);
void performInitialPermutation(int inBlock[], int outBlock[]);
void perform16Rounds(int inBlock[],int inKey[]);
void shiftKey(int key[], int round, int index);
void performKeyPermutation(int inKey[], int outKey[]);
void performKeyCompression(int inKey[], int outKey[]);
void performExpansionPermutation(int inBlock[], int outBlock[]);
void processSBox(int inBlock[], int outBlock[]);
void processPBox(int inBlock[], int outBlock[]);
void performFinalPermutation(int inBlock[], int outBlock[]);
void convertBitsToChar(int outputBits[],char output[]);
void writeText(string text,ostream& output, int textLength);

int main (int argc, char *argv[])
	{
	ifstream input;
	string plainText;
	string inFileName;
	ofstream outFile;
	string outFileName;
	string keyword;
	int length;
	int keyWordLength;
	int keyInBits[64];
	int keyForRounds[56];
	int messageInBits[64];
	int permutedMessage[64];
	int outputBits[64];
	char output[8];
	string outputString;
	outputString = "";
	if(argc < 3)
		{
		cout << "Command should be in the form: \ndesencrypt keyword plaintext-file [ciphertext-file]" << endl;
		exit(1);
		}
	if(argc == 3)
		{
		for(int n = 0; argv[2][n] != '\0' && argv[2][n] != '.'; n++)
			{
			if(argv[2][n] != '.')
				outFileName = outFileName + argv[2][n];
			else
				break;
			}
		outFileName = outFileName + ".DES\0";
		}
	else if(argc == 4)
		outFileName = argv[3];
	inFileName = argv[2];
	keyword = argv[1];
	for(keyWordLength = 0; keyword[keyWordLength]; keyWordLength++)
		{
		if(isupper(keyword[keyWordLength]))
			keyword[keyWordLength] - 'A' + 'a';
		}
	if(keyWordLength != 8)
		{
		cout << "Keyword needs to be 8 characters" << endl;
		exit(1);
		}
	input.open(inFileName.c_str());
	if(input.fail())
		{
		cout << "File not found" << endl;
		exit(1);
		}
	length = getPlainText(input, plainText);
	input.close();
	for(int n = 0; n < length; n = n + 8)
		{
		convertKeyToArray(keyword,keyInBits,0);
		convertMessageToArray(plainText,messageInBits,n,n + 8);
		performInitialPermutation(messageInBits,permutedMessage);
		performKeyPermutation(keyInBits,keyForRounds);
		perform16Rounds(permutedMessage,keyForRounds);
		performFinalPermutation(permutedMessage,outputBits);
		convertBitsToChar(outputBits,output);
		for(int m = 0; m < 8; m++)
			outputString = outputString + output[m];
		}
	outputString = outputString + '\0';
	outFile.open(outFileName.c_str());
	writeText(outputString,cout,length);
	writeText(outputString,outFile,length);
	outFile.close();
	}
//=====================================================
//Reads input file into plainText string
int getPlainText(ifstream& input, string& plainText)
	{
	int length;
	char temp;
	plainText = "";
	length = 0;
	while(input.get(temp))
		{
		plainText = plainText + temp;
		length++;
		}
	plainText = plainText + '\0';
	return length;
	}
//=====================================================
//Stores key in binary into keyInBits[]
void convertKeyToArray(string key, int keyInBits[],int start)
	{
	int mask;
	for(int n = 0; n < 8; n++)
		{
		mask = 64;
		for(int m = 0; m < 7; m++)
			{
			keyInBits[n * 8 + m] = key[start + n] / mask;
			key[start + n] = key[start + n] % mask;
			mask = mask / 2;
			}
		keyInBits[n * 8 + 7] = 0;
		}
	}
//=====================================================
//Stores part of message in binary into messageInBits[]
void convertMessageToArray(string message, int messageInBits[],int start, int end)
	{
	int mask;
	for(int n = 0; n < 8; n++)
		{
		mask = 128;
		for(int m = 0; m < 8; m++)
			{
			messageInBits[n * 8 + m] = message[start + n] / mask;
			message[start + n] = message[start + n] % mask;
			mask = mask / 2;
			}
		}
	}
//=====================================================
//Performs initial permutation on inBlock[]
void performInitialPermutation(int inBlock[], int outBlock[])
	{
	int initialPermutationBlock[4][16] = {58,50,42,34,26,18,10,2,60,52,44,36,28,20,12,4,
									62,54,46,38,30,22,14,6,64,56,48,40,32,24,16,8,
									57,49,41,33,25,17,9,1,59,51,43,35,27,19,11,3,
									61,53,45,37,29,21,13,5,63,55,47,39,31,23,15,7};

	for(int n = 0; n < 4; n++)
		for(int m = 0; m < 16; m++)
			outBlock[n * 16 + m] = inBlock[initialPermutationBlock[n][m]-1];

	}
//=====================================================
//Calls functions for the 16 rounds
void perform16Rounds(int inBlock[],int inKey[])
	{
	int rightData[48];
	int shortKey[48];
	int xorData[48];
	int leftData[32];
	int pBoxResult[32];
	int swapVal;
	for(int n = 0; n < 16; n++)
		{
		shiftKey(inKey,n,0);
		shiftKey(inKey,n,28);
		performKeyCompression(inKey,shortKey);
		performExpansionPermutation(inBlock, rightData);
		for(int m = 0; m < 48; m++)
			xorData[m] = shortKey[m] ^ rightData[m];
		processSBox(xorData,leftData);
		processPBox(leftData,pBoxResult);
		for(int m = 0; m < 32; m++)
			inBlock[m] = inBlock[m] ^ pBoxResult[m];
		if(n!=15)
			for(int m = 0; m < 32; m++)
				{
				swapVal = inBlock[m];
				inBlock[m] = inBlock[m + 32];
				inBlock[m + 32] = swapVal;
				}
		}
	}
//=====================================================
//shifts bits by 1 or 2 in key[]
void shiftKey(int key[], int round, int index)
	{
	int keyShifts[16] = {1,1,2,2,2,2,2,2,1,2,2,2,2,2,2,1};
	if(keyShifts[round] == 1)
		{
		int first;
		first = key[index];
		for(int n = index; n < index + 28; n++)
			key[n] = key[n + 1];
		key[index + 27] = first;
		}
	else
		{
		int first;
		int second;
		first = key[index];
		second = key[index + 1];
		for(int n = index; n < index + 26; n++)
			key[n] = key[n + 2];
		key[index + 26] = first;
		key[index + 27] = second;
		}
	}
//=====================================================
//Performs key permutation on inKey[]
void performKeyPermutation(int inKey[], int outKey[])
	{
	int keyPermutation[4][14] = {57,49,41,33,25,17,9,1,59,50,42,34,26,18,
								10,2,59,51,43,35,27,19,11,3,60,52,44,36,
								63,55,47,39,31,23,15,7,62,54,46,38,30,22,
								14,6,61,53,45,37,29,21,13,5,28,20,12,4};

	for(int n = 0; n < 4; n++)
		for(int m = 0; m < 14; m++)
			outKey[n * 14 + m] = inKey[keyPermutation[n][m]-1];
	}
//=====================================================
//Performs key compression on inKey[]
void performKeyCompression(int inKey[], int outKey[])
	{
	int compressionPermutationBlock[4][12] = {14,17,11,24,1,5,3,28,15,6,21,10,
										23,19,12,4,26,8,16,7,27,20,13,2,
										41,52,31,37,47,55,30,40,51,45,33,48,
									44,49,39,56,34,53,46,42,50,36,29,32};

	for(int n = 0; n < 4; n++)
		for(int m = 0; m < 12; m++)
			outKey[n * 12 + m] = inKey[compressionPermutationBlock[n][m]-1];
	}
//=====================================================
//Performs expansion permutation on inBlock[]
void performExpansionPermutation(int inBlock[], int outBlock[])
	{
	int expansionPermutationBlock[4][12] = {32,1,2,3,4,5,4,5,6,7,8,9,
										8,9,10,11,12,13,12,13,14,15,16,17,
										16,17,18,19,20,21,20,21,22,23,24,25,
										24,25,26,27,28,29,28,29,30,31,32,1};

	for(int n = 0; n < 4; n++)
		for(int m = 0; m < 12; m++)
			outBlock[n * 12 + m] = inBlock[expansionPermutationBlock[n][m]-1 + 32];
	}
//=====================================================
//Handles S-Box permutations
void processSBox(int inBlock[], int outBlock[])
	{
	int sBox1[4][16] = {14,4,13,1,2,15,11,8,3,10,6,12,5,9,0,7,
						0,15,7,4,14,2,13,1,10,6,12,11,9,5,3,8,
						4,1,14,8,13,6,2,11,15,12,9,7,3,10,5,0,
						15,12,8,2,4,9,1,7,5,11,3,14,10,0,6,13};

	int sBox2[4][16] = {5,1,8,14,6,11,3,4,9,7,2,13,12,0,5,10,
						3,13,4,7,15,2,8,14,12,0,1,10,6,9,11,5,
						0,14,7,11,10,4,13,1,5,8,12,6,9,3,2,15,
						13,8,10,1,3,15,4,2,11,6,7,12,0,5,14,9};

	int sBox3[4][16] = {10,0,9,14,6,3,15,5,1,13,12,7,11,4,2,8,
						13,7,0,9,3,4,6,10,2,8,5,14,12,11,15,1,
						13,6,4,9,8,15,3,0,11,1,2,12,5,10,14,7,
						1,10,13,0,6,9,8,7,4,15,14,3,11,5,2,12};

	int sBox4[4][16] = {7,13,14,3,0,6,9,10,1,2,8,5,11,12,4,15,
						13,8,11,5,6,15,0,3,4,7,2,12,1,10,14,9,
						10,6,9,0,12,11,7,13,15,1,3,14,5,2,8,4,
						3,15,0,6,10,1,13,8,9,4,5,11,12,7,2,14};

	int sBox5[4][16] = {2,12,4,1,7,10,11,6,8,5,3,15,13,0,14,9,
						14,11,2,12,4,7,13,1,5,0,15,10,3,9,8,6,
						4,2,1,11,10,13,7,8,15,9,12,5,6,3,0,14,
						11,8,12,7,1,14,2,13,6,15,0,9,10,4,5,3};

	int sBox6[4][16] = {12,1,10,15,9,2,6,8,0,13,3,4,14,7,5,11,
						10,15,4,2,7,12,9,5,6,1,13,14,0,11,3,8,
						9,14,15,5,2,8,12,3,7,0,4,10,1,13,11,6,
						4,3,2,12,9,5,15,10,11,14,1,7,6,0,8,13};

	int sBox7[4][16] = {4,11,2,14,15,0,8,13,3,12,9,7,5,10,6,1,
						13,0,11,7,4,9,1,10,14,3,5,12,2,15,8,6,
						1,4,11,13,12,3,7,14,10,15,6,8,0,5,9,2,
						6,11,13,8,1,4,10,7,9,5,0,15,14,2,3,12};

	int sBox8[4][16] = {13,2,8,4,6,15,11,1,10,9,3,14,5,0,12,7,
						1,15,13,8,10,3,7,4,12,5,6,11,0,14,9,2,
						7,11,4,1,9,12,14,2,0,6,10,13,15,3,5,8,
						2,1,14,7,4,10,8,13,15,12,9,0,3,5,6,11};
	int row;
	int column;
	int result;
	int blockPosition;
	blockPosition = 0;
	for(int n = 0; n < 32; n = n + 4)
		{
		row = inBlock[blockPosition] * 2 + inBlock[blockPosition + 5];
		column = inBlock[blockPosition + 1] * 8 + inBlock[blockPosition + 2] * 4 + inBlock[blockPosition + 3] * 2 + inBlock[blockPosition + 4];
		if(blockPosition == 0)
			result = sBox1[row][column];
		else if(blockPosition == 6)
			result = sBox2[row][column];
		else if(blockPosition == 12)
			result = sBox3[row][column];
		else if(blockPosition == 18)
			result = sBox4[row][column];
		else if(blockPosition == 24)
			result = sBox5[row][column];
		else if(blockPosition == 30)
			result = sBox6[row][column];
		else if(blockPosition == 36)
			result = sBox7[row][column];
		else if(blockPosition == 42)
			result = sBox8[row][column];
		outBlock[n] = result / 8;
		result = result % 8;
		outBlock[n + 1] = result / 4;
		result = result % 4;
		outBlock[n + 2] = result / 2;
		result = result % 2;
		outBlock[n + 3] = result / 1;
		blockPosition = blockPosition + 6;
		}
	}
//=====================================================
//Handles P-Box permutations
void processPBox(int inBlock[], int outBlock[])
	{
	int pBox[2][16] = {16,7,20,21,29,12,28,17,1,15,23,26,5,18,31,10,
						2,8,24,14,32,27,3,9,19,13,30,6,22,11,4,25};

	for(int n = 0; n < 2; n++)
		for(int m = 0; m < 16; m++)
			outBlock[n * 16 + m] = inBlock[pBox[n][m]-1];
	}
//=====================================================
//Performs final permutation
void performFinalPermutation(int inBlock[], int outBlock[])
	{
	int finalPermutation[4][16] = {40,8,48,16,56,24,64,32,39,7,47,15,55,23,63,31,
									38,6,46,14,54,22,62,30,37,5,45,13,53,21,61,29,
									36,4,44,12,52,20,60,28,35,3,43,11,51,19,59,27,
									34,2,42,10,50,18,58,26,33,1,41,9,49,17,57,25};
	for(int n = 0; n < 4; n++)
		for(int m = 0; m < 16; m++)
			outBlock[n * 16 + m] = inBlock[finalPermutation[n][m]-1];
	}
//=====================================================
//Displays the text on screen or writes to file
void convertBitsToChar(int outputBits[],char output[])
	{
	int conversion;
	for(int n = 0; n < 8; n++)
		{
		conversion = 128;
		for(int m = 0; m <8; m++)
			{
			output[n] = output[n] + outputBits[n * 8 + m] * conversion;
			conversion = conversion/2;
			}
		}
	}
//=====================================================
//Displays the text on screen or writes to file
void writeText(string text,ostream& output, int textLength)
	{
	for(int n = 0; n < textLength; n++)
		output << text[n];
	}