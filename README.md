# DES

[DES](https://gitlab.com/bamathis/DES) is an implemention of DES encryption.

## Quick Start

### Program Execution

#### Encrypting
```
$ ./desencrypt.cpp 
```
#### Decrypting
```
$ ./desdecrypt.cpp 
```


### Known Issues

- The given input file must only consist of letters and spaces. 
- Encrypts and decrypts text to console correctly but adds characters that messup decryption when writing to file.
